use failure::Error;
use serde_json;
use ureq;

use std::env;
use std::fs::{self, File};
use std::os::unix::fs::symlink;
use std::path::Path;

use skim::{Skim, SkimOptions, SkimOptionsBuilder, SkimOutput};
use std::collections::HashMap;
use std::io::Cursor;

// ~/.steam/bin32 is a symlink to ~/.local/share/Steam/ubuntu12_32 (by default)
const STEAM_DEPOTS_PATH: &str = ".steam/bin32/steamapps/content";

pub fn create_depot_symlink(
    appid: &str,
    depot_id: &str,
    appname: &str,
    depotname: &str,
) -> std::io::Result<()> {
    if !Path::new(appname).is_dir() {
        fs::create_dir(appname)?;
    }

    let src = Path::new(&env::var("HOME").unwrap())
        .join(STEAM_DEPOTS_PATH)
        .join(format!("app_{}", appid))
        .join(format!("depot_{}", depot_id));

    let dst = format!("{}/{}", appname, depotname);
    // println!("{} -> {}", src, dst);
    symlink(&src, &dst)
}

/// Returns map of <appid, appname> from JSON object fetched from Steam Store WEB API
pub fn get_app_map(json: serde_json::Value) -> Result<HashMap<String, String>, Error> {
    let map = json
        .as_object()
        .unwrap()
        .get("applist")
        .unwrap()
        .get("apps")
        .unwrap()
        .as_array()
        .unwrap();

    let mut apps_map: HashMap<String, String> = HashMap::with_capacity(map.len());

    let apps_map = map
        .iter()
        .map(|x| {
            (
                x.get("appid").unwrap().as_u64().unwrap().to_string(),
                x.get("name").unwrap().as_str().unwrap().to_string(),
            )
        })
        .collect();

    Ok(apps_map)
}

/// Fuzzy menu to select Steam appid(s)
pub fn app_menu(apps_map: &HashMap<String, String>, query: &str) -> Vec<String> {
    let options = SkimOptionsBuilder::default()
        .header(Some("Select an app"))
        // .query(Some("219150")) // Hotline Miami
        .query(Some(query))
        .build()
        .unwrap();

    let skim_menu_items = apps_map
        .iter()
        // .iter()
        .map(|(k, v)| format!("{:<8} {}", k, v))
        .collect::<Vec<String>>()
        .join("\n")
        .to_string();

    let selected_apps = Skim::run_with(&options, Some(Box::new(Cursor::new(skim_menu_items))))
        .map(|out| out.selected_items)
        .unwrap_or_else(Vec::new);

    selected_apps
        .iter()
        .map(|x| {
            x.get_output_text()
                .split_whitespace()
                .nth(0)
                .unwrap()
                .to_string()
        })
        .collect()
}

/// Fuzzy menu to select depots
pub fn depot_menu(depots_map: &[super::Depot]) -> Vec<String> {
    let options = SkimOptionsBuilder::default()
        .header(Some("Select depots(s) [use TAB to select several items]"))
        .multi(true)
        // .regex(true)
        // .query(Some("(?i).*soundtrack|ost|flac|wav|mp3*"))
        .build()
        .unwrap();

    let menu = depots_map
        .iter()
        .map(|d| format!("{:<8} {} ({})", d.id, d.name, humansize(d.maxsize)))
        .collect::<Vec<String>>()
        .join("\n")
        .to_string();

    let selected_depots = Skim::run_with(&options, Some(Box::new(Cursor::new(menu))))
        .map(|out| out.selected_items)
        .unwrap_or_else(Vec::new);

    selected_depots
        .iter()
        .map(|x| {
            x.get_output_text()
                .split_whitespace()
                .nth(0)
                .unwrap()
                .to_string()
        })
        .collect()
}

static UNITS: &[&str] = &["", "Ki", "Mi", "Gi", "Ti"];

/// Returns human-readable size for given number of `bytes`
pub fn humansize(bytes: u64) -> String {
    let mut bytes = bytes as f64;
    for unit in UNITS {
        if bytes < 1024.0 {
            return format!("{:.2} {}B", bytes, unit);
        }
        bytes /= 1024.0;
    }

    format!("{:.2} PiB", bytes)
}

#[cfg(test)]
mod tests {
    use super::humansize;
    #[test]
    fn test_humansize_123_mib() {
        assert_eq!(humansize(123 * 1024 * 1024 + 456 * 1024), "123.45 MiB");
    }
}
