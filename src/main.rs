#![allow(unused)]

#[macro_use]
extern crate failure;
use failure::Error;

use select::document::Document;
use select::predicate::{Attr, Name, Predicate};
use ureq;

use std::collections::HashMap;
use std::fmt::{self, Display};
use std::fs::{self, File};
use std::io::BufReader;
use std::path::Path;
use std::process::{Command, Stdio};

use autopilot;

use yansi::Paint;

mod util;
use util::*;

pub struct Depot {
    id: u64,
    name: String,
    maxsize: u64,
    parent: u64,
}

impl Display for Depot {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{} ::: {} ::: {}",
            self.id,
            self.name,
            humansize(self.maxsize)
        )
    }
}

/// Parses SteamDB page for a list of depots
fn get_depots_steamdb(appid: u64) -> Result<Vec<Depot>, Error> {
    let url = format!("https://steamdb.info/app/{}/depots/", appid);

    let resp = ureq::get(&url).call();

    println!(
        "[{}] Fetching {} ... {}",
        Paint::cyan("INFO"),
        Paint::cyan(&url),
        if resp.ok() {
            Paint::green("OK")
        } else {
            Paint::red("failed :(")
        }
    );

    let doc = Document::from_read(resp.into_reader()).unwrap();

    let depots_div = match doc.find(Attr("id", "depots")).next() {
        Some(val) => val,
        _ => bail!(
            "[{}] No depots for appid {} found. Check {} for details.",
            Paint::yellow("WARN"),
            Paint::cyan(appid),
            Paint::cyan(url),
        ),
    };
    let depots_table = depots_div.find(Name("table")).next().expect("table fail");

    let mut depots: Vec<Depot> = Vec::new();

    for table_row in depots_table.find(Name("tbody").descendant(Name("tr"))) {
        let mut cells = table_row.find(Name("td"));

        let depot_id = cells.next().expect("depot_id").text().parse::<u64>()?;
        let depot_name = cells.next().expect("depot_name").text();
        let depot_maxsize = cells
            .next()
            .expect("depot_maxsize")
            .attr("data-sort")
            .unwrap()
            .parse::<u64>()?;

        depot_name.trim();

        depots.push(Depot {
            id: depot_id,
            name: depot_name,
            maxsize: depot_maxsize,
            parent: appid,
        })
    }

    Ok(depots)
}

fn get_json(url: &str) -> Result<serde_json::Value, Error> {
    Ok(ureq::get(url).call().into_json()?)
}

fn read_json_from_file<P: AsRef<Path>>(path: P) -> Result<serde_json::value::Value, Error> {
    Ok(serde_json::from_reader(BufReader::new(File::open(path)?))?)
}

fn main() -> Result<(), Error> {
    let filename = "/tmp/steam_store_applist.json";

    let json = if Path::new(filename).exists() {
        read_json_from_file(filename).expect("Failed to read json")
    } else {
        let steam_store_applist = "https://api.steampowered.com/ISteamApps/GetAppList/v0002";

        let resp_body = ureq::get(steam_store_applist).call().into_string()?;
        fs::write(filename, &resp_body)?;
        serde_json::from_str(&resp_body)?
    };

    let apps_map: HashMap<_, _> = get_app_map(json)?;
    let query = std::env::args().skip(1).collect::<Vec<String>>().join(" ");
    let selected_apps = app_menu(&apps_map, &query);

    for appid in selected_apps.iter() {
        let depots = match get_depots_steamdb(appid.parse::<u64>()?) {
            Ok(val) => val,
            Err(e) => {
                println!("{}", e.to_string());
                continue;
            }
        };

        let depots_map: HashMap<_, _> = depots
            .iter()
            .map(|d| (d.id.to_string(), d.name.to_owned()))
            .collect();

        let selected_depots = depot_menu(&depots);

        for depot_id in selected_depots.iter() {
            match create_depot_symlink(&appid, &depot_id, &apps_map[appid], &depots_map[depot_id]) {
                Ok(_) => (),
                Err(e) => {
                    println!("{}", format_err!("Symlink: {}", e));
                }
            }

            Command::new("steam")
                .arg("steam://open/console")
                .stdout(Stdio::null())
                .spawn()
                .expect("Failed to open Steam Console");

            std::thread::sleep(std::time::Duration::from_secs(2));

            autopilot::key::type_string(
                &format!("download_depot {} {}\n", appid, depot_id),
                &[],
                0.,
                0.,
            );
        }
    }

    Ok(())
}
