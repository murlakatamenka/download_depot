# download_depot.

`download_depot` is an interactive CLI utility that eases downloading of separate Steam depots.
It's written in Rust :crab:

It relies on

- [`SteamDB`](https://steamdb.info): to fetch depot information
- Steam Client: to download depots (alternative backends, such as [DepotDownloader](https://github.com/SteamRE/DepotDownloader) or [steamctl](https://github.com/ValvePython/steamctl) are possible)

## Usage

`download_depot <gamename>` or simply `download_depot`

The application treats any input as a search query, there is no need to quote anything, for example:

`download_depot hotline miami 2`

## Building from source

### Rust toolchain

[Rust toolchain](https://rustup.rs/) is required to build. Relevant Arch Wiki is [here](https://wiki.archlinux.org/index.php/Rust#Installation) (don't worry, it's just a couple of minutes!).

TL;DR

```sh
pacman -S rust
```

or

```sh
pacman -S rustup
rustup install stable
```

### Building / Installation

```sh
cargo build --release --locked
```

Built binary can be found in `target/release`. It's all you need.

You can also build and install a binary to a specified destination (provided you have permissions for the latter):

```sh
cargo install --path . --root ~/.local/bin
```

### Troubleshooting

#### Tiling window managers

If Steam client is assigned to a certain workspace/tag of a tiling window manager, it is recommended to use the utility from terminal within the same workspace/tag.
